

#ifndef _BLENDER_H_
#define _BLENDER_H_

// #include "Arduino.h"
#include "AudioStream48.h"
#include "control_adau1372.h"

#include <EiconPreampStructs.h>

class EiconPreamp : public AudioStream
{
public:
	EiconPreamp(void) : AudioStream(4, inputQueueArray) { }
	
	void pre_setup(machine_control_t* tCon)
	{
		inCon=&tCon->ee.mc.inctrl; // .adc_level[0];
		
		tunerfilters=&tCon->mc.pc.tune[0].b0;
		ch0filters=&tCon->mc.pc.ch0[0].b0;
		ch1filters=&tCon->mc.pc.ch0[1].b0;
		ch2filters=&tCon->mc.pc.ch0[2].b0;
		blend1filter=&tCon->mc.pc.bl1.b0;
		tonefilters=&tCon->mc.pc.tone[0].b0;
		// finalfilters=&tCon->mc.pc.ff[0].b0; -* catered in tonefilters by a tricky guy.
	}
	
	virtual void update(void);

	void setVolume(float newVal=0); // mutes if none sent
	void setTunerGain(float newVal=1); // 100% if none sent
	void setBlend0Gain(uint8_t channel, float newVal); // no default available.
	void setBlend1Gain(uint8_t channel, float newVal); // no default available.
	
	uint8_t counter(uint8_t n, bool reset=false);

	float peakCh0(bool reset=true);
	float peakCh1(bool reset=true);
	float peakCh2(bool reset=true);
	float peakTune(bool reset=true);
	float peakOut(bool reset=true);

private:
	audio_block_t *inputQueueArray[4];
//	uint32_t* settings=NULL;
//	float* level=NULL;
	int16_t silence[128]={ };
	float fbuf0[128]={ };
	float fbuf1[128]={ };
	
	input_control_t *inCon=NULL;
	
	float *tunerfilters=NULL, *ch0filters=NULL, *ch1filters=NULL, *ch2filters=NULL, *blend1filter=NULL, *tonefilters=NULL;
	
	float out0gain=0;
	float out1gain=0;
	float blend0gain[2]={0,0};
	float blend1gain[2]={0,0};

	typedef struct __attribute__((packed)) {
		float peaklo;
		float peakhi;
		float peak;
	} peak_info_t;
	
	typedef struct __attribute__((packed)) {
		peak_info_t root={5,0,65535}; // num_peak_infos, dummy, divisor.
		peak_info_t ch0={32767,-32768,0};
		peak_info_t ch1={32767,-32768,0};
		peak_info_t ch2={32767,-32768,0};
		peak_info_t out={32767,-32768,0};
		peak_info_t tune={32767,-32768,0};
	} peak_group_t;
	
	peak_group_t peaks;
	
	uint8_t counters[4]={3,0,0,0}; // first item is count of counters, handier for asm
	
	
};





#endif

