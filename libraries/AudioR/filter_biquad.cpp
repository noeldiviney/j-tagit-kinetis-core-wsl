/* Audio Library for Teensy 3.X
 * Copyright (c) 2014, Paul Stoffregen, paul@pjrc.com
 *
 * Development of this audio library was funded by PJRC.COM, LLC by sales of
 * Teensy and Audio Adaptor boards.  Please support PJRC's efforts to develop
 * open source software by purchasing Teensy or other PJRC products.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice, development funding notice, and this permission
 * notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#include "filter_biquad.h"
#include "utility/dspinst.h"

#if defined(KINETISK)

 #if HAS_FPU_SO_USE_IT==1
	#warning got me an FPU, hopefully this is worthwhile.

#define SUCH_FUN 1
	
void AudioFilterBiquad::update(void)
{
	audio_block_t *block;
#if SUCH_FUN==1
	register
#endif
	bqfvartype sum,b0,b1,b2,a1,a2,inSample, bprev1, bprev2, aprev1, aprev2;
	int16_t *data, *end;
	bqfvartype *state,*rstate;
	uint8_t counter=0,flag=0;
	block = receiveWritable();
	if (!block) return;

	end=block->data+AUDIO_BLOCK_SAMPLES;
	state=(bqfvartype *)definition;
	
	do {
		b0 = *state++;
		b1 = *state++;
		b2 = *state++;
		a1 = *state++;
		a2 = *state++;
		rstate=state;
		bprev1=*state++;
		bprev2=*state++;
		aprev1=*state++;
		aprev2=*state++;
#if defined(SUCH_FUN)
		state++;
#else
		sum=*state++;
#endif
		data=end-AUDIO_BLOCK_SAMPLES; 
		do {
			inSample=(bqfvartype)*data;
#if defined(SUCH_FUN)
//			sum=inSample*b0;
			asm volatile("vmul.f32 %0, %1, %2" : "=s" (sum) : "s" (sample), "s" (b0));
//			sum+=bprev1*b1;
			asm volatile("vlma.f32 %0, %1, %2" : "+s" (sum) : "s" (bprev1), "s" (b1));
//			sum+=aprev1*a1;
			asm volatile("vlma.f32 %0, %1, %2" : "+s" (sum) : "s" (aprev1), "s" (a1));
//			sum+=aprev2*a2;
			asm volatile("vlma.f32 %0, %1, %2" : "+s" (sum) : "s" (aprev2), "s" (a2));
			aprev2=aprev1;
			aprev1=sum+bprev2*b2;
#else
			sum+=inSample*b0;
			sum+=bprev1*b1;
			sum+=bprev2*b2;
			sum+=aprev1*a1;
			sum+=aprev2*a2;
			aprev2=aprev1;
			aprev1=sum;
			sum-=aprev1; // fair enough to say =0 but oddly subtraction may execute a little faster than direct assignment. Go figure.
#endif
			bprev2=bprev1;
			bprev1=inSample;
			*data++=saturate16((int32_t)aprev1); // *data++=(int16_t)aprev1;
		} while (data<end);
		*rstate++=bprev1;
		*rstate++=bprev2;
		*rstate++=aprev1;
		*rstate++=aprev2;
#if !defined(SUCH_FUN)
		*rstate=sum;
#endif
		counter++;
		flag=(stage_flags&(1<<counter))>>counter; // 1, 2, 3, 4
	} while(flag);
	transmit(block);
	release(block);
}

 #else

#warning Using biquad with direct asm
void AudioFilterBiquad::update(void)
{
	audio_block_t *block;
	int64_t sum=0;
	int32_t b0,b1,b2,a1,a2,flag,descaled=-1073741824;
	int32_t inSample, bprev1, bprev2, aprev1, aprev2;
	int16_t *data, *end, *tmp, *tmp2;
	int32_t *state;

	block = receiveWritable();
	if (!block) return;

	end=block->data+AUDIO_BLOCK_SAMPLES;
	state=(int32_t *)definition;
	
	do {
		b0 = *state++;
		b1 = *state++;
		b2 = *state++;
		a1 = *state++;
		a2 = *state++;
		tmp=tmp2=(int16_t *)state;
		bprev1=(int32_t)*tmp++;
		bprev2=(int32_t)*tmp++;
		state++;
		aprev1=(int32_t)*tmp++;
		aprev2=(int32_t)*tmp++;
		state++;
		sum=((int64_t) *state) & 0x3FFFFFFF;
		data=end-AUDIO_BLOCK_SAMPLES;
		do {
			inSample=(int32_t)*data;
			asm volatile("smlal %Q0, %R0, %1, %2" : "+r" (sum) : "r" (inSample), "r" (b0));
			asm volatile("smlal %Q0, %R0, %1, %2" : "+r" (sum) : "r" (bprev1), "r" (b1));
			asm volatile("smlal %Q0, %R0, %1, %2" : "+r" (sum) : "r" (bprev2), "r" (b2));
			asm volatile("smlal %Q0, %R0, %1, %2" : "+r" (sum) : "r" (aprev1), "r" (a1));
			asm volatile("smlal %Q0, %R0, %1, %2" : "+r" (sum) : "r" (aprev2), "r" (a2));
			aprev2=aprev1;
			aprev1=sum>>30;
			*data++=saturate16(aprev1); // *data++=(int16_t)aprev1;
			asm volatile("smlal %Q0, %R0, %1, %2" : "+r" (sum) : "r" (aprev1), "r" (descaled));
			bprev2=bprev1;
			bprev1=inSample;
		} while (data<end);
		*tmp2++=(int16_t)bprev1;
		*tmp2++=(int16_t)bprev2;
		*tmp2++=(int16_t)aprev1;
		*tmp2++=(int16_t)aprev2;
		flag=*state & 0x80000000;
		*state++=((int32_t)sum & 0x3FFFFFFF) | flag;
	} while(flag);
	transmit(block);
	release(block);
}

 #endif

#elif defined(KINETISL)

void AudioFilterBiquad::update(void)
{
        audio_block_t *block;

	block = receiveReadOnly();
	if (block) release(block);
}

#endif

#if HAS_FPU_SO_USE_IT==1

void AudioFilterBiquad::setCoefficients(uint8_t stage, const bqfvartype *coefficients)
{
	if (stage > 3) return;
	bqfvartype *dest = definition + (stage *10);
	__disable_irq();
	stage_flags|=1<<stage;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++ * -1;
	*dest++ = *coefficients++ * -1;
	dest += 4;
	*dest = 0; // this is the 'sum' and probably not a good idea to have it here.
	__enable_irq();
}

void AudioFilterBiquad::jetCoefficients(uint8_t stage, const bqfvartype *coefficients)
{
	if (stage >= 4) return;
	bqfvartype *dest = definition + (stage *10);
	stage_flags|=1<<stage;
	__disable_irq();
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++ * -1;
	*dest++ = *coefficients++ * -1;
	dest+=4; // do not clear state.
	*dest   = 0;
	__enable_irq();
}


void AudioFilterBiquad::setCoefficients_(uint8_t stage, const bqfvartype *coefficients)
{
	if (stage >= 4) return;
	bqfvartype *dest = definition + (stage *10);
	stage_flags|=1<<stage;
	__disable_irq();
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = 0;
	*dest++ = 0; // state is being cleared here.
	*dest++ = 0;
	*dest++ = 0;
	*dest   = 0;
	__enable_irq();
}


#else
	

void AudioFilterBiquad::setCoefficients(uint32_t stage, const int *coefficients)
{
	if (stage >= 4) return;
	int32_t *dest = definition + (stage << 3);
	__disable_irq();
	if (stage > 0) *(dest - 1) |= 0x80000000;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++ * -1;
	*dest++ = *coefficients++ * -1;
	//*dest++ = 0;
	//*dest++ = 0;  // clearing filter state causes loud pop
	dest += 2;
	*dest   &= 0x80000000;
	__enable_irq();
}

void AudioFilterBiquad::jetCoefficients(uint32_t stage, const int *coefficients)
{
	if (stage >= 4) return;
	int32_t *dest = definition + (stage << 3);
	__disable_irq();
	if (stage > 0) *(dest - 1) |= 0x80000000;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++ * -1;
	*dest++ = *coefficients++ * -1;
/*	*dest++;
	*dest++; */
	dest+=2;
	*dest   &= 0x80000000;
	__enable_irq();
}


void AudioFilterBiquad::setCoefficients_(uint32_t stage, const int *coefficients)
{
	if (stage >= 4) return;
	int32_t *dest = definition + (stage << 3);
	__disable_irq();
	if (stage > 0) *(dest - 1) |= 0x80000000;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = *coefficients++;
	*dest++ = 0;
	*dest++ = 0;
	*dest   &= 0x80000000;
	__enable_irq();
}

#endif

