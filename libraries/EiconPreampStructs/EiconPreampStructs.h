

#ifndef _EiconPreampStructs_h_
#define _EiconPreampStructs_h_

#include "audiostream48.h"
// #include "EiconPreampStructs.S"

#ifdef __cplusplus
extern "C" {
#endif

float gasmAbsf(float num);
int32_t gasmAbs32(int32_t num);
int32_t gasmTest1(void);

#ifdef __cplusplus
}
#endif


/******************************************* defines and structs for overall control purposes ****************************************************/

#ifndef COEFFICIENTS_FLOAT_T
struct coefficients_float_t {
	float b0;
	float b1;
	float b2;
	float a1;
	float a2;
	float aprev1;
	float aprev2;
	float bprev1;
	float bprev2;
	float sum;
};
#define FILTER_COEFS_PROMISE 32767
#define FILTER_COEFS_LOAD_CONTD(b0,b1,b2,a1,a2) {b0,b1,b2,a1,a2,0,0,0,0,FILTER_COEFS_PROMISE}
#define FILTER_COEFS_LOAD_FINISH(b0,b1,b2,a1,a2) {b0,b1,b2,a1,a2,0,0,0,0,0}

#define FILTER_COEFS_PASS_CONTD {1,0,0,0,0,0,0,0,0,32767}
#define FILTER_COEFS_PASS_FINISH {1,0,0,0,0,0,0,0,0,0}

#define COEFFICIENTS_FLOAT_T FILTER_COEFS_FINISH
#endif

/*
#ifndef FILTER_TYPES_E
enum filter_types_e { // enumeration as part of this object should be non-conflictive with other declarations similar.
	FLT_LOPASS=0,
	FLT_HIPASS,
	FLT_BANDPASS,
	FLT_NOTCH,
	FLT_PARAEQ,
	FLT_LOSHELF,
	FLT_HISHELF,
	FLT_NONE
};
#define FILTER_TYPES_E
#endif
*/


// Everything required for a 'profile'

/*Type filter_control_t
	f as single
	q as single
	d as single
	t as byte
	reserved(2) as byte
End Type*/
typedef struct __attribute__((packed)) {
	float f;
	float q;
	float d;
	uint8_t t; // this is in byte order to pick up as *uint8_t, *uint16_t or *uint32_t - helpful if you might write ASM to make coefficients some day.
	uint8_t reserved[3]; // killing 3 bytes here, get over it.
} filter_control_t;
#define FILTER_CONTROL_T {1000,0.7071,0,FLT_NONE}
#define FILTER_CONTROL_T2 {FILTER_CONTROL_T,FILTER_CONTROL_T}
#define FILTER_CONTROL_T3 {FILTER_CONTROL_T,FILTER_CONTROL_T,FILTER_CONTROL_T}
#define FILTER_CONTROL_T4 {FILTER_CONTROL_T,FILTER_CONTROL_T,FILTER_CONTROL_T,FILTER_CONTROL_T}
#define FILTER_CONTROL_T5 {FILTER_CONTROL_T,FILTER_CONTROL_T,FILTER_CONTROL_T,FILTER_CONTROL_T,FILTER_CONTROL_T}

// filter_control_t test_filter_control_t[4]=FILTER_CONTROL_T4;

/*Type level_bounds_t
	dBmin as single
	dBmax as single
End Type*/
typedef struct __attribute__((packed)) {
	float dBmin;
	float dBmax;
} level_bounds_t;
#define LEVEL_BOUNDS_T {0,0}
#define LEVEL_BOUNDS_T60_0 {-60,0}
// level_bounds_t test_level_bounds_t=LEVEL_BOUNDS_T;

/*Type blend_control_t
	c0b as level_bounds_t
	c0f(1) as filter_control_t
	c1b as level_bounds_t
	c1f(1) as filter_control_t
End Type*/
typedef struct __attribute__((packed)) {
	level_bounds_t ch0b;
	filter_control_t ch0f[2];
	level_bounds_t ch1b;
	filter_control_t ch1f[2];
} blend_control_t;
#define BLEND_CONTROL_T {LEVEL_BOUNDS_T,FILTER_CONTROL_T2,LEVEL_BOUNDS_T,FILTER_CONTROL_T2}
// blend_control_t test_blend_control_t=BLEND_CONTROL_T;
/* Paint a profile here, whole thing, proper figures everywhere.
{
	{0,0},{

}
*/
/*Type tone_control_t
	b(1) as filter_control_t
	m(1) as filter_control_t
	t(1) as filter_control_t
End Type*/
typedef struct __attribute__((packed)) {
	filter_control_t b[2];
	filter_control_t m[2];
	filter_control_t t[2];
} tone_control_t;
#define TONE_CONTROL_BASS {{FLT_PARAEQ,110,0.7,-20},{FLT_PARAEQ,110,0.7,0}}
#define TONE_CONTROL_MIDS {{FLT_PARAEQ,330,0.7,-20},{FLT_PARAEQ,330,0.7,0}}
#define TONE_CONTROL_TREB {{FLT_PARAEQ,880,0.7,-20},{FLT_PARAEQ,880,0.7,0}}
#define TONE_CONTROL_T {TONE_CONTROL_BASS,TONE_CONTROL_MIDS,TONE_CONTROL_TREB}
//tone_control_t test_tone_control_t=TONE_CONTROL_T;

/*Type profile_control_t
	blend(1) as blend_control_t
	tone as tone_control_t
	ch0f(3) as filter_control_t
	ch1f(3) as filter_control_t
	ch2f(3) as filter_control_t
	ff(3) as filter_control_t
End Type*/
typedef struct __attribute__((packed)) {
	blend_control_t blend[2];
	tone_control_t tone;
	filter_control_t ch0f[4];
	filter_control_t ch1f[4];
	filter_control_t ch2f[4];
	filter_control_t ff[4];
	level_bounds_t volctrl;
} profile_control_t;
#define PROFILE_CONTROL_T {{BLEND_CONTROL_T,BLEND_CONTROL_T},TONE_CONTROL_T,FILTER_CONTROL_T4,FILTER_CONTROL_T4,FILTER_CONTROL_T4,FILTER_CONTROL_T4,{LEVEL_BOUNDS_T60_0,LEVEL_BOUNDS_T60-0}};
// profile_control_t test_profile_control_t=PROFILE_CONTROL_T;


// Everything required for the machine

/*Type input_control_t
	adc_level(3) as byte
	pga_ctrl(3) as byte
	channel(3) as byte
End Type*/
typedef struct __attribute__((packed)) {
	uint8_t adc_level[4]; // set to 255 to disable?
	uint8_t pga_ctrl[4]; // [7:0]->[7] PGA Enable; [6] PGA Boost10+; [5:0] PGA Level
	uint8_t channel[4]; // [7:0]->[7:6] reserved [5] Enabled [4] Inverted [3:0] Source selection {0-input0, 1-input1, 2-input2, 3-input3, 4,5,6,7}
} input_control_t; // 12 bytes / 4 = 3
#define INPUT_CONTROL_T {{0,0,0,255},{128|26,128|64|25,128|27},{32|0,32|1,32|2,0}}

/*Type level_control_t
	volume as single
	blend0 as single
	blend1 as single
	bass as single
	mids as single
	treb as single
End Type*/
typedef struct __attribute__((packed)) {
	float volume;
	float blend0;
	float blend1;
	float bass;
	float mids;
	float treb;
} level_control_t;
#define LEVEL_CONTROL_T {0.75,0.25,0.25,0.5,0.5,0.5}

/*Type tuner_control_t
	minMatch as byte
	minPairs as byte
	tolerance as single
	scope as single
	thresh as single
	target as single
	filter(2) as filter_control_t
	res as integer
End Type*/
typedef struct __attribute__((packed)) {
	uint8_t minMatch; // recommend 6
	uint8_t minPairs; // recommend 5
	float tolerance;
	float scope;
	float thresh;
	float target; // 0.00372549f*(n+1); range 0.00372549 to 0.95372549 - recommend 0.7
	filter_control_t filter[2];
	uint16_t res;
} tuner_control_t;
#define TUNER_CONTROL_T {6,5,0.0333,0.01,0.1,0.7,{{FLT_LOPASS,880,0.7071,0},{FLT_HIPASS,40,0.7071,0}}}

/*Type battery_control_t
	full(1) as single
	empty(1) as single
	swlow as single
End Type*/
typedef struct __attribute__((packed)) {
	float full[2];
	float empty[2];
	float swlow;
} battery_control_t;
#define BATTERY_CONTROL_T {{9,4.5},{7,3.3},5.1}
// battery_control_t test_battery_control_t=BATTERY_CONTROL_T;

/*Type encoder_control_t
	tpc as byte
	w_ms as integer
	res as byte
End Type*/
typedef struct __attribute__((packed)) {
	uint8_t tpc; // ticks per click, used as divisor for encoder.read ops.
	uint8_t w_bc; // (n+1) milliseconds between reads, effects acceleration - consider audio blocks instead?
	uint8_t bc; // block count per item in level_control_t for soft stepping purpose
	uint8_t step; // which item in level_control_t will be stepped this pass
	float ssv; // soft step value in dB
} encoder_control_t;
#define ENCODER_CONTROL_T {4,30,10,0,0.5}

/*Type display_control_t
	timeout as byte
	nofresh as byte
	logo_seq(3) as byte
	res as integer
End Type*/
typedef struct __attribute__((packed)) {
	uint8_t timeout; // timeout*100ms
	uint8_t logoout; // logo timeout * 100ms
	uint8_t nofresh; // block refresh=nofresh*10ms
	uint8_t logo_seq[4];
	uint8_t res; // just realigning to 4 byte boundary
} display_control_t;
#define DISPLAY_CONTROL_T {50,10,{1,2,3,4}, }


/* Type main_machine_t
	inctrl as input_control_t
	tuner as tuner_control_t
	volume as level_bounds_t
	virtcent as single
	enc as encoder_control_t
	pots as level_control_t
	batt as battery_control_t
	disp as display_control_t
	ch0name(11) as byte
	ch1name(11) as byte
	ch2name(11) as byte
End Type*/
typedef struct __attribute__((packed)) {
	input_control_t inctrl; // mCon.ee.mc.inctrl...
	tuner_control_t tuner;
	float virtcent;
	encoder_control_t enc;
	level_control_t pots;
	battery_control_t batt;
	display_control_t disp;
	char ch0name[12];
	char ch1name[12];
	char ch2name[12];
} main_machine_t;
#define MAIN_MACHINE_T { }

/*Type eeprom_content_t
	prf(3) as profile_control_t
	mc as main_machine_t
	cprf as byte
	resn(2) as byte
	res(212) as long
	logo(1023) as byte
End Type*/
typedef struct __attribute__((packed)) {
	uint32_t validation_unit;
	profile_control_t prf[4];
	main_machine_t mc;
	uint8_t cprf; // current profile...
	uint8_t resn[3];
	uint32_t res[205]; // I will surely find more things to shove into main_machine_t, or something.
	uint32_t logo[256];
} eeprom_content_t;

// Private (~ish)

// filter_landings_t
typedef struct __attribute__((packed)) {
//	uint32_t fupdates; // [7] ALL [6] tone[2] [5] tone[1] [4] tone[0] [3] bl1[1] [2] bl1[0] [1] bl0[1] [0] bl0[0]
	filter_control_t bl0[2];
	filter_control_t bl1[2];
	filter_control_t tone[3];
} filter_landings_t;

//packed_coefficients_t
typedef struct __attribute__((packed)) {
	coefficients_float_t ch0[5]={FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_FINISH};
	coefficients_float_t ch1[5]={FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_FINISH};
	coefficients_float_t ch2[5]={FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_FINISH};
	coefficients_float_t bl1=FILTER_COEFS_PASS_FINISH;
	coefficients_float_t tone[3]={FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_CONTD}; // Experimental - 1 filter bank, 7 filter ops.
	coefficients_float_t ff[4]={FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_FINISH};
	coefficients_float_t tune[2]={FILTER_COEFS_PASS_CONTD,FILTER_COEFS_PASS_FINISH};
} packed_coefficients_t;

// ghost_machine_t
typedef struct __attribute__((packed)) {
	uint8_t mac_state;
	uint8_t ctrl_mode;
	uint8_t disp_mode;
	uint8_t res; // realign to 4 byte boundaries.
	
	level_control_t pots0; // pots0 chases pots in main_machine_t
	level_control_t pots1; // pots1 use to set chased value, set to -1 to cause filter updates where applicable?

	filter_landings_t fp;
	packed_coefficients_t pc; // mCon.mc.pc..
	
} ghost_machine_t;

// machine_control_t
typedef struct __attribute__((packed)) {
	eeprom_content_t ee={ };
	profile_control_t pf;
	ghost_machine_t mc;
} machine_control_t;





/*** dB Calc ***/
#define dB2ratio(n) exp(n/10)
#define ratio2dB(n) (n>0 ? 10*log(n) : -999)
//#define ratio2dB(n) (n>0 ? -999 : 10*log(n))
// Enumeration
enum pin_self_latch_enum {pin_self_latch_off=0,pin_self_latch_on};
enum pin_slapper_enable_enum {pin_slapper_enable_off=0,pin_slapper_enable_on};

enum complete_pin_set_enum { // CONFLICTS!! // pin_n = arduino pin // (arduino_pin#/arduino_analog#|mk2#?X_pin#)
  pin_0=0, // (0|39)
  pin_1, // (1|40)
  pin_encoder_right_a, // pin_2, // (2|57)
  pin_3, // (3|28)
  pin_4, // (4|29)
  pin_encoder_left_b, // pin_5, // (5|64)
  pin_encoder_middle_b, // pin_6, // (6|61)
  pin_encoder_right_e, // pin_7, // (7|59)
  pin_encoder_middle_a, // pin_8, // (8|60)
  pin_i2s_bclk, // pin_9, // (9|46)
  pin_10, // (10|49)
  pin_slapper_output, // PIN_I2S_MCLK  11 // pin_11, // (11|51)
  pin_self_latch, // pin_12, // (12|52)
  pin_i2s_pico_0, // pin_13, // (13|50)
  pin_encoder_right_b, // pin_14, // (14/A0|58)
  pin_encoder_middle_e, // PIN_I2S_POCI_1 // pin_15, // (15/A1|43)
  pin_16, // (16/A2|35)
  pin_17, // (17/A3|36)
  pin_i2c_sda0, // pin_18, // (18/A4|38)
  pin_i2c_scl0, // pin_19, // (19/A5|37)
  pin_oled_sa0, // pin_20, // (20/A6|62)
  pin_encoder_left_a, // pin_21, // (21/A7|63)
  pin_i2s_poci_0, // pin_22, // (22/A8|44)
  pin_i2s_lrclk, // pin_23, // (23/A9|45)
  pin_slapper_enable, // pin_24, // (24|27)
  pin_25, // (25|42)
  pin_usb_volts_detect, // pin_26, // (26/A15|2)
  pin_oled_reset, // pin_27, // (27/A16|54)
  pin_jack_sense, // pin_28, // (28/A17|53)
  pin_codec_shutdown, // pin_29, // (29/A18|55)
  pin_i2s_pico_1, // pin_30, // (30/A19|56)
  pin_encoder_left_e, // pin_31, // (31/A20|1)
  pin_32, // (32|41)
  pin_33, // (33|26)
  pin_vin_sense, // pin_a10, // (34|9)
  pin_a11, // (35|10)
  pin_a12, // (36|11)
  pin_a13, // (37|12)
  pin_null_1, // (38)
  pin_null_2, // (39)
  pin_a14, // (40|18)
};

#define PIN_TIE_DOWN_LIST {pin_0, pin_1, pin_3, pin_4, pin_10, pin_16, pin_17, pin_25, pin_32, pin_33, /*pin_a11, pin_a12, pin_a13, pin_a14,*/ 255}

/* enum class filter_types_e : unsigned char {
	fltLoPass=0,
	fltHiPass,
	fltBandPass,
	fltNotch,
	fltParaEQ,
	fltLoShelf,
	fltHiShelf,
	fltNone
}; fuck stupid bitch enum, compiler is a fuckwit about this */

#define fltLoPass 0
#define fltHiPass 1
#define fltBandPass 2
#define fltNotch 3
#define fltParaEQ 4
#define fltLoShelf 5
#define fltHiShelf 6
#define fltNone

// Handies...


float fmap(float x, float in_min, float in_max, float out_min, float out_max);
float dBmap(float x, float out_min, float out_max, float virtcentre0);
filter_control_t mfilter(float portion, filter_control_t flt1, filter_control_t flt2);
void calcBiquadf(uint8_t type, float Fc, float Q, float peakGain, float * coefs, float Fs=48000);

#endif


